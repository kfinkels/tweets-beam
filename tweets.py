import json

import apache_beam as beam
from apache_beam.io import WriteToText, fileio
from apache_beam.options.pipeline_options import PipelineOptions
from apache_beam.transforms.trigger import AccumulationMode, Repeatedly, AfterWatermark
from apache_beam.transforms.window import FixedWindows, TimestampedValue
from beam_nuggets.io import kafkaio

from transforms.WIndowTransform import AddWindowingInfoFn
from transforms.filter_users import FilterUsers

kafka_topic = "trump"
kafka_config = {"topic": kafka_topic,
                "bootstrap_servers": "localhost:9092"}


def print_tweets():
    with beam.Pipeline(options=PipelineOptions()) as p:
        tweets = p | "Reading messages from Kafka" >> kafkaio.KafkaConsume(kafka_config)
        tweets | 'Writing to stdout' >> beam.Map(print)


def fetch_tweet(_, tweet):
    return json.loads(tweet)


def is_valid_user(tweet):
    return tweet.get('user', {}).get('id') is not None or \
           ('followers_count' in tweet and tweet.get('followers_count') > 0)


def is_friendly(tweet):
    return 'friends_count' in tweet['user'] and tweet['user'].get('friends_count') > 100


def by_location(tweet, num_of_partitions):
    return 0 if tweet.get('user', {}).get('location') else 1


def file_names(*args):
    file_name = fileio.destination_prefix_naming()(*args)
    destination, *_ = file_name.split("----")
    return f"{destination}.json"


class JsonSink(fileio.TextSink):
    def write(self, element):
        _, data = element
        self._fh.write(json.dumps(data).encode("utf8"))
        self._fh.write("\n".encode("utf8"))


def destination(element):
    window, _ = element
    return str(int(window.start.micros / 1000))


# code1 - using filter
def filter_none_users_or_no_followers_using_filter():
    with beam.Pipeline(options=PipelineOptions()) as p:
        p | "Reading messages from Kafka" >> kafkaio.KafkaConsume(kafka_config) | \
            'Fetch tweet' >> beam.MapTuple(fetch_tweet) | \
            'Filter users' >> beam.Filter(is_valid_user) | \
            'Writing to stdout' >> beam.Map(print)


# code2 - using pardo
def filter_none_users_or_no_followers_using_pardo():
    with beam.Pipeline(options=PipelineOptions()) as p:
        p | "Reading messages from Kafka" >> kafkaio.KafkaConsume(kafka_config) | \
            'Fetch tweet' >> beam.MapTuple(fetch_tweet) | \
            'Filter users' >> beam.ParDo(FilterUsers()) | \
            'Writing to stdout' >> beam.Map(print)


# code3 - using pardo and filter
def filter_unfriendly_users():
    with beam.Pipeline(options=PipelineOptions()) as p:
        p | "Reading messages from Kafka" >> kafkaio.KafkaConsume(kafka_config) | \
            'Fetch tweet' >> beam.MapTuple(fetch_tweet) | \
            'Filter users' >> beam.ParDo(FilterUsers()) | \
            'Filter unfriendly' >> beam.Filter(is_friendly) | \
            'Writing to stdout' >> beam.Map(print)


# code4 - writing to a file
def filtered_tweets_to_file():
    with beam.Pipeline(options=PipelineOptions()) as p:
        p | "Reading messages from Kafka" >> kafkaio.KafkaConsume(kafka_config) | \
            'Fetch tweet' >> beam.MapTuple(fetch_tweet) | \
            'Filter users' >> beam.ParDo(FilterUsers()) | \
            'Filter unfriendly' >> beam.Filter(is_friendly) | \
            'Writing to file' >> WriteToText('output/tweets.json')


# code5 - writing to files using partition
def filtered_tweets_with_partition():
    with beam.Pipeline(options=PipelineOptions()) as p:
        location, no_location = p | "Reading messages from Kafka" >> kafkaio.KafkaConsume(kafka_config) | \
            'Fetch tweet' >> beam.MapTuple(fetch_tweet) | \
            'Filter users' >> beam.ParDo(FilterUsers()) | \
            'Filter unfriendly' >> beam.Filter(is_friendly) | \
            'Partition' >> beam.Partition(by_location, 2)

        location | 'Writing to lang file' >> WriteToText('output/location.json')
        no_location | 'Writing to no lang file' >> WriteToText('output/no_location.json')


# code6 - writing to files using windowing
def timestamp_by_event_time():
    with beam.Pipeline(options=PipelineOptions()) as p:
        p | "Reading messages from Kafka" >> kafkaio.KafkaConsume(kafka_config) | \
            'Add Windows' >> beam.WindowInto(FixedWindows(60), Repeatedly(AfterWatermark()),
                                             accumulation_mode=AccumulationMode.DISCARDING) | \
            'Fetch tweet' >> beam.MapTuple(fetch_tweet) | \
            'Filter users' >> beam.ParDo(FilterUsers()) | \
            'Filter unfriendly' >> beam.Filter(is_friendly) | \
            'Add Timestamps' >> beam.Map(lambda x: TimestampedValue(x, int(x['timestamp_ms']))) | \
            'Add Window Info' >> beam.ParDo(AddWindowingInfoFn()) | \
            'Write Files' >> fileio.WriteToFiles(path="output/", sink=lambda dest: JsonSink(),
                                                 destination=destination)


if __name__ == '__main__':
    # 1: using filter
    filter_none_users_or_no_followers_using_filter()
    # 2: using pardo
    # filter_none_users_or_no_followers_using_pardo()
    # 3: using pardo and filter
    # filter_unfriendly_users()
    # 4: writing to a file
    # filtered_tweets_to_file()
    # 5: writing to files using partition (location\no location)
    # filtered_tweets_with_partition()
    # 6: writing to files using windowing
    # timestamp_by_event_time()
