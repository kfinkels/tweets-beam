import apache_beam as beam


class FilterUsers(beam.DoFn):

    def process(self, element):
        if element.get('user', {}).get('id') is not None or \
               ('followers_count' in element and element.get('followers_count') > 0):
            yield element
        return
