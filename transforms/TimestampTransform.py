import apache_beam as beam

class GetTimestamp(beam.DoFn):


  def process(self, element, timestamp=beam.DoFn.TimestampParam):
    yield timestamp.to_utc_datetime(), element
