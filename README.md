# Documentation
https://beam.apache.org/documentation/programming-guide/


# Steps
* Source: read messages from kafka topic
* Definition: Junk tweets are tweets by users with less than 100 followers.
  Task: Filter out nulls, deleted tweets (tweets with field "delete"), and junk tweets (see definition)
* Sink: print tweets to console
* Definition: usa influencers - followers_count>100k & user.location.contains(usa)
  Task: Add a sink that writes filtered usa influencers and writes them to json file.
* Add partitioning to the sink - by the users location
* Side-outputs: keep raw tweets in the stream, and put another copy in a usaInfluencers side-output stream
* Sink: for each stream create a sink that writes the stream into json files.
    * for the usaInfluencers stream use the tweet's user location as partition
